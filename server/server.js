require('dotenv').config();
console.log(__dirname);
const express = require('express'),
      multer = require('multer'),
      path = require('path'),
      cors = require('cors');

const storage = multer.diskStorage({
    destination: (req,file,callback)=>{
        callback(null, path.join(__dirname, 'uploads'))
    },
    filename: (req, file, callback)=>{
        console.log(file);
        callback(null, Date.now() + '-' + file.originalname);
    }
})
const upload = multer({storage:storage});
const app = express();

const APP_PORT = process.env.PORT;

app.use(cors());

app.post('/upload', upload.single('myphoto'), (req,res, next)=>{
    console.log(req.file);
    console.log(req.body);
    res.status(200).json({message: 'OK'});
});

app.listen(APP_PORT, ()=>{
    console.log(`App server started at ${APP_PORT}`);
})